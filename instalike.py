#!/usr/bin/python
# -*- coding: utf-8 -*-

from selenium import webdriver
import logging
import time
import random
from datetime import datetime

class InstaLike:
    ''' Instalike main class with all stuff in it. Just use.
        Google Chrome required and some others libs. (Selenium with driver)'''

    _paths_ = {
        'xpath_login' : '//*[@id="react-root"]/section/main/div/article/div/div[1]/div/form/div[3]/button',
        'xpath_first_photo' : '//*[@id="react-root"]/section/main/div/div[3]/article/div/div/div[1]/div[1]',
        'xpath_like_button' : '/html/body/div[3]/div/div[2]/div/article/div[2]/section[1]/span[1]/button',
        'xpath_next_photo_first' : '/html/body/div[3]/div/div[1]/div/div/a',
        'xpath_next_photo_default' : '/html/body/div[3]/div/div[1]/div/div/a[2]'
    }

    def __init__(self, username, password, depth = 1, targets = [], baseDelay = 3):
        self.username = username
        self.depth = depth
        self.targets = targets
        self.baseDelay = baseDelay
        logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', level=logging.INFO)
        logging.info('Instalike started')
        self._driver_ = webdriver.Chrome()
        self._driver_.get('https://www.instagram.com/accounts/login/')
        self._wait_()
        self._driver_.find_element_by_name('username').send_keys(username)
        self._driver_.find_element_by_name('password').send_keys(password)
        self._driver_.find_element_by_xpath(InstaLike._paths_['xpath_login']).click()
        self._wait_()
        logging.info('Logged in as ' + username)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._driver_.quit()

    def _wait_(self):
        time.sleep(self.baseDelay + random.randint(1, 2))

    def _findFirst_(self, nickName):
        self._driver_.get('https://instagram.com/' + nickName)
        self._wait_()
        logging.info('Working for ' + self._driver_.title)
        try:
            self._driver_.find_element_by_xpath(InstaLike._paths_['xpath_first_photo']).click()
            self._wait_()
            return True
        except:
            logging.warning('Can\'t find first photo of ' + nickName + '. May be profile is empty or closed.')
            return False

    def _likeOne_(self, number):
        likeButton = self._driver_.find_element_by_xpath(self._paths_['xpath_like_button'])
        try:
            # Just another way... to die
            # spanGrey = self._driver_.find_element_by_class_name('glyphsSpriteHeart__outline__24__grey_9')
            spanRed = None
            try:
                spanRed = self._driver_.find_element_by_class_name('glyphsSpriteHeart__filled__24__red_5')
            except:
                pass
            if spanRed is not None:
                raise Exception('LIKE is already toggled')
            likeButton.click()
            self._wait_()
            logging.info('Photo ' + str(number) + ' from last liked')
            return True
        except Exception as e:
            logging.warning('Photo ' + str(number) + ' from last NOT liked: ' + e.message)
            return False

    def _getNext_(self):
        try:
            self._driver_.find_element_by_xpath(self._paths_['xpath_next_photo_default']).click()
            self._wait_()
            return True
        except:
            try:
                self._driver_.find_element_by_xpath(self._paths_['xpath_next_photo_first']).click()
                self._wait_()
                return True
            except:
                logging.info('Can\'t get next photo. All are supposed to be done.')
                return False

    def start(self):
        startPoint = datetime.now().replace(microsecond = 0)
        for target in self.targets:
            if self._findFirst_(target):
                count = 0
                for i in xrange(1, self.depth + 1):
                    if self._likeOne_(i):
                        count += 1
                    if not self._getNext_():
                        break
                logging.info('Liked ' + str(count) + ' photos from ' + target + ' account')
        endPoint = datetime.now().replace(microsecond = 0)
        logging.info('Time were bounded: ' + str(endPoint - startPoint))
        logging.info('Nox!')

if __name__ == "__main__":
    print 'Hi! It\'s InstaLike module.\n Solemnly swear that you are up to no good!\n Enjoy!'